if __name__ == '__main__':
    # print('hi')
    from meolearn_database_tools import *
    arguments = sys.argv
    word = arguments[1]
    word = word.lower()
    frequency = get_frequency(word, database_name)

    if is_word_exist(frequency):
        word_content = do_word_exist_tasks(word, frequency, database_name)

    else:
        do_word_does_not_exist_tasks(word, frequency, database_name)
