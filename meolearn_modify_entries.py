from meolearn_database_tools import *
from create_meolearn_database import database_name


def get_current_word_content(word, frequency, database_name):
    current_word_folder = get_current_possible_word_folder_name(
        word, frequency, database_name)

    word_path = get_updated_word_path(word, current_word_folder)
    current_word_content = read_word_content(word_path)
    return current_word_content


def modify_entry(word, new_word_file_content, frequency, database_name):
    current_word_folder = get_current_possible_word_folder_name(
        word, frequency, database_name)

    word_path = get_updated_word_path(word, current_word_folder)

    save_word_content(new_word_file_content, word_path)


if __name__ == '__main__':
    while True:
        word = input('(-(-_(-_-)_-)-) Insert your word: ')

        word = word.lower()

        frequency = get_frequency(word, database_name)

        if is_word_exist(frequency):
            current_word_content = get_current_word_content(
                word, frequency, database_name)

            print(f'(+_+) current {word} content in database is: ')
            print(current_word_content)

            user_choice = input(
                'do you want to modify this (i only understand yes): ')
            if user_choice == 'yes':
                new_content = input(f'type new content for {word}: ')
                modify_entry(word, new_content, frequency, database_name)

                print(f'(^_^) {word} content modified successfully!!!\n')
            else:
                print('(^_^) try another word')

        else:
            print(
                f'[O_o] this word is not in database, you should first insert {word} in database\n'
            )
