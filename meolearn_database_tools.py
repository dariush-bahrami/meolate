import os
import sys
from meriam_webster_api import pprint_word_data
from create_meolearn_database import database_name


def is_database_exist(database_name):
    return os.path.isdir(database_name)


def create_database(database_name):
    if not os.path.isdir(database_name):
        os.mkdir(database_name)


def get_frequency(word, database_name):
    all_words = os.listdir(database_name)

    # removing frequency prefix
    formated_words = {word[6:]: word[:3] for word in all_words}

    if word in formated_words:
        return int(formated_words[word]) + 1
    else:
        return 1


def get_current_possible_word_folder_name(word, frequency, database_name):
    return f'{database_name}/{frequency - 1:0>3} - {word}'


def get_updated_word_folder_name(word, frequency, database_name):
    return f'{database_name}/{frequency:0>3} - {word}'


def is_word_exist(frequency):
    if frequency == 1:
        return False
    else:
        return True


def create_word_directory(word_folder_name):
    os.mkdir(word_folder_name)


def update_word_directory(current_word_folder, updated_folder_name):
    os.rename(current_word_folder, updated_folder_name)


def get_updated_word_path(word, updated_word_folder_path):
    return f'{updated_word_folder_path}/{word}.txt'


def read_word_content(word_path):
    with open(word_path, mode='r') as open_file:
        word_content = open_file.read()
    return word_content ## TODO


def save_word_content(word_file_content, word_path):
    with open(word_path, mode='w', encoding='utf-8') as open_file:
        open_file.write(word_file_content)
        
def do_word_exist_tasks(word, frequency, database_name):
    current_word_folder = get_current_possible_word_folder_name(
        word, frequency, database_name)

    updated_word_folder = get_updated_word_folder_name(word, frequency,
                                                       database_name)

    update_word_directory(current_word_folder, updated_word_folder)

    pprint_word_data(word, updated_word_folder, updated_word_folder, width=60)


def do_word_does_not_exist_tasks(word, frequency, database_name):
    word_folder_name = get_updated_word_folder_name(word, frequency,
                                                    database_name)
    create_word_directory(word_folder_name)
    
    pprint_word_data(word, word_folder_name, word_folder_name, width=60)
