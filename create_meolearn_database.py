database_name = 'Meolearn Database'

if __name__ == '__main__':
    from meolearn_database_tools import is_database_exist, create_database
    if not is_database_exist(database_name):
        create_database(database_name)