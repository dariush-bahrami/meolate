import requests
import json
from pathlib import Path
import os


def get_api_key(api_data_path="meriam_webster_api_data.json"):
    with open(api_data_path, "r") as open_file:
        api_data = json.load(open_file)
    dictionary_key = api_data['Key (Dictionary)']
    thesaurus_key = api_data['Key (Thesaurus)']
    return dictionary_key, thesaurus_key


def request_dictionary(word, save_folder='Dictionary Request Results'):
    word = word.lower()
    api_key = get_api_key()[0]
    language = 'en'

    url = ('https://www.dictionaryapi.com/api/v3/references/collegiate/json/' +
           f'{word}?key={api_key}')

    r = requests.get(url)
    save_path = Path.joinpath(Path(save_folder), Path(f'dictionary_{word}.json'))
    if not Path(save_folder).is_dir():
        os.mkdir(Path(save_folder))
    with open(save_path, mode='w', encoding="utf-8") as file:
        file.write(json.dumps(r.json()))
    
    return r.json()

def request_thesaurus(word, save_folder='Thesaurus Request Results'):
    word = word.lower()
    api_key = get_api_key()[1]
    language = 'en'

    url = ('https://www.dictionaryapi.com/api/v3/references/thesaurus/json/' +
           f'{word}?key={api_key}')

    r = requests.get(url)
    save_path = Path.joinpath(Path(save_folder), Path(f'thesaurus_{word}.json'))
    if not Path(save_folder).is_dir():
        os.mkdir(Path(save_folder))
    with open(save_path, mode='w', encoding="utf-8") as file:
        file.write(json.dumps(r.json()))

    return r.json()

def dictionary_json_loader(word, save_folder='Dictionary Request Results'):
    word = word.lower()
    save_path = Path.joinpath(Path(save_folder), Path(f'dictionary_{word}.json'))
    try:
        with open(save_path, mode='r', encoding="utf-8") as stream:
            data = json.load(stream)
    except FileNotFoundError:
        data = request_dictionary(word, save_folder=save_folder)
    return data

def thesaurus_json_loader(word, save_folder='Thesaurus Request Results'):
    word = word.lower()
    save_path = Path.joinpath(Path(save_folder), Path(f'thesaurus_{word}.json'))
    try:
        with open(save_path, mode='r', encoding="utf-8") as stream:
            data = json.load(stream)
    except FileNotFoundError:
        data = request_thesaurus(word, save_folder=save_folder)
    return data

def get_dictionary_data(word, save_folder='Dictionary Request Results'):
    word = word.lower()
    word_data = dictionary_json_loader(word,save_folder=save_folder)
    
    definitions_list = [] # List of list
    for result in word_data:
        definitions_list.append([definition for definition in result['shortdef']])
    

        
    return definitions_list

def pprint_dictionary_data(word, width=60, save_folder='Dictionary Request Results'):
    from pprint import pprint
    definitions_list = get_dictionary_data(word, save_folder=save_folder)
    print()
    print('*'*width)
    title = f'Dictionary Data for {word}'
    print(f'*{title:^58}*')
    print('*'*width)
    print('\n' + '='*width + '\n')
    for result_list in definitions_list:
        for meaning in result_list:
            pprint(meaning, width=width )
        print('\n' + '='*width + '\n')
        
def get_thesaurus_data(word, save_folder='Thesaurus Request Results'):
    word = word.lower()
    word_data = thesaurus_json_loader(word, save_folder=save_folder)
    if type(word_data[0]) != str:
        definitions = []
        for result in word_data:
            definitions.append([definition for definition in result['shortdef']])
    else:
        definitions = [result for result in word_data]    
    return definitions

def pprint_thesaurus_data(word, width=60, save_folder='Dictionary Request Results'):
    from pprint import pprint
    definitions_list = get_thesaurus_data(word, save_folder=save_folder)
    if type(definitions_list[0]) != str:
        print()
        print('*'*width)
        title = f'Thesaurus Data for {word}'
        print(f'*{title:^58}*')
        print('*'*width)
        print('\n' + '='*width + '\n')
        for result_list in definitions_list:
            for meaning in result_list:
                pprint(meaning, width=width )
            print('\n' + '='*width + '\n')
    else:
        print()
        print('*'*width)
        title = f'No Useful Thesaurus Data for {word}'
        print(f'*{title:^58}*')
        print('*'*width)
        



def pprint_word_data(word, dictionary_save_folder,
                     thesaurus_save_folder, width=60):
    pprint_thesaurus_data(word, width=width, save_folder=thesaurus_save_folder)
    pprint_dictionary_data(word, width=width, save_folder=dictionary_save_folder)
    
